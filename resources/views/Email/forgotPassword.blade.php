@component('mail::message')
# Forgot your password ?

That's okay, it happens! Click on the button below to reset your password.

@component('mail::button', ['url' => "#"])
{{ $token }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
