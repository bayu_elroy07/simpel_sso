@component('mail::message')
# Verify Email

Please press the button below to confirm an email.

@component('mail::button', ['url' => url('email-verified?token='.$token)])
CONFIRM EMAIL
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
