<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module_category_documents extends Model
{
    use HasFactory;

    protected $fillable = [
        'module_categories_id', 'mcd_nama','status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];


    public function module_categories()
    {
        return $this->belongsTo(Module_categories::class);
    }
}
