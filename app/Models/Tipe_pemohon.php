<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipe_pemohon extends Model
{
    use HasFactory;
    protected $table = "tipe_pemohon";
    protected $fillable = [
        "dp_nama",
        "status"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    public function dokumen_pemohon()
    {
        return $this->hasMany(Dokumen_pemohon::class);
    }


}
