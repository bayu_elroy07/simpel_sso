<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    use HasFactory;

    protected $fillable = [
        'm_nama',
        'm_ditjen',
        'm_kategori',
        'status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function module_categories()
    {
        return $this->hasMany(Module_categories::class);
    }

}
