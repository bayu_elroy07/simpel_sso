<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tanaman extends Model
{
    use HasFactory;
    protected $table = "tanaman";

    protected $fillable =   [
        "tanaman_kategori_id",
        "tanaman_kategori_nama",
        "tanaman_kategori_sub_id",
        "tanaman_kategori_sub_nama",
        "t_nama",
        "t_nama_latin",
        "t_slug",
        "status"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    public function tanaman_kategori_sub()
    {
        return $this->belongsTo(Tanaman_kategori_sub::class);
    }

    public function tanaman_kategori()
    {
        return $this->belongsTo(Tanaman_kategori::class);
    }


}
