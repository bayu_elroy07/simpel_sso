<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    use HasFactory;
    protected $table = "kota";
    protected $fillable = [
        "k_nama",
        "provinsi_id"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    public function kecamatan()
    {
        return $this->hasMany(Provinsi::class);
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class);
    }
}
