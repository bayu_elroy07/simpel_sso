<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kbli_perusahaan extends Model
{
    use HasFactory;
    protected $table = "kbli_perusahaan";
    protected $fillable = [
        "kp_nama",
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];
}
