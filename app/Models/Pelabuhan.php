<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelabuhan extends Model
{
    use HasFactory;
    protected $table = "pelabuhan";
    protected $fillable = [
        "pl_nama",
        "pl_code_nama",
        "pl_code",
        "pl_lokasi",
        "pl_ncode",
        "pl_ps_code",
        "status"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];
}
