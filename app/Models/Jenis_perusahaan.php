<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jenis_perusahaan extends Model
{
    use HasFactory;
    protected $table = "jenis_perusahaan";
    protected $fillable = [
        "jp_nama",
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

}
