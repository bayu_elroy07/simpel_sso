<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Negara extends Model
{
    use HasFactory;
    protected $table = "negara";
    protected $fillable = [
        "n_inisial",
        "n_nama",
        "status"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];
}
