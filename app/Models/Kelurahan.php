<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    use HasFactory;
    protected $table = "kelurahan";
    protected $fillable = [
        "kl_nama",
        "kecamatan_id"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class);
    }
}
