<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tujuan_penggunaan_benih extends Model
{
    use HasFactory;
    protected $table = "tujuan_penggunaan_benih";
    protected $fillable = [
        "tpb_nama",
        "status"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];
}
