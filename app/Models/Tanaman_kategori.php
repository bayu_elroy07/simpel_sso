<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tanaman_kategori extends Model
{
    use HasFactory;
    protected $table = "tanaman_kategori";
    protected $fillable =   [
                                "tk_nama",
                                "status"
                            ];

    protected $hidden =     [
                                "created_at",
                                "updated_at"
                            ];

    public function tanaman_kategori_sub()
    {
        return $this->hasMany(Tanaman_kategori_sub::class);
    }

    public function tanaman()
    {
        return $this->hasMany(Tanaman::class);
    }

}
