<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dokumen_pemohon extends Model
{
    use HasFactory;
    protected $table = "dokumen_pemohon";
    protected $fillable = [
        "dp_nama",
        "tipe_pemohon_id",
        "status"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    public function tipe_pemohon()
    {
        return $this->belongsTo(Tipe_pemohon::class);
    }
}
