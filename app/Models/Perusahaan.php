<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    use HasFactory;
    protected $table = "perusahaan";
    protected $fillable = [
        'nib',
        'p_nama',
        'p_npwp',
        'p_penanggung_jawab',
        'p_email',
        'p_telp',
        'p_alamat',
        'p_json',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
