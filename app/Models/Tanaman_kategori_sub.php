<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tanaman_kategori_sub extends Model
{

    use HasFactory;
    protected $table = "tanaman_kategori_sub";

    protected $fillable =   [
        "tanaman_kategori_id",
        "tanaman_kategori_nama",
        "tanaman_kategori_sub_id",
        "tanaman_kategori_sub_nama",
        "tks_nama",
        "status"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];
    public function tanaman()
    {
        return $this->hasMany(Tanaman::class);
    }

    public function tanaman_kategori()
    {
        return $this->belongsTo(Tanaman_kategori::class);
    }
}
