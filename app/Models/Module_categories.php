<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module_categories extends Model
{
    use HasFactory;

    protected $fillable = [
        'modules_id', 'mc_nama','status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function modules()
    {
        return $this->belongsTo(Modules::class);
    }

    public function module_category_documents()
    {
        return $this->hasMany(Module_category_documents::class);
    }
}
