<?php

namespace App\Http\Controllers\API\CRUD;

use App\Models\Tanaman;
use App\Models\Tanaman_kategori;
use App\Models\Tanaman_kategori_sub;
use App\Http\Controllers\Controller;
use App\Helpers\ResponseFormatter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TanamanController extends Controller
{
    public function index(Request $request)
    {
        try {
            $req = $request->all();

            $columns = [];
            $error = [];

            // foreach($req as $key => $val){

            //     if (Schema::hasColumn('module', $key)) array_push($columns, $key);
            //     else  array_push($error, "Column ".$key." is exist");

            // }

            $search = $request->search;
            $list = Tanaman::with(['tanaman_kategori'=>function($s) use ($search){
                if($search){
                    $list->where("tks_nama",'LIKE','%'.$search.'%');

                }
            },'tanaman_kategori_sub'=>function($s) use ($search){
                if($search){
                    $list->where("tk_nama",'LIKE','%'.$search.'%');

                }
            }])->where('status',true);

            $url = "";

            if($request->id){
                $list->where("id",$request->id);
            }

            if($request->search){
                    // $list->where("id",'LIKE','%'.$request->search.'%');

                    $list->where("t_nama",'LIKE','%'.$request->search.'%');
                    $list->orWhere("t_nama_latin",'LIKE','%'.$request->search.'%');
                    $list->orWhere("t_slug",'LIKE','%'.$request->search.'%');
                    if($url=="") $url = "?search=".$request->search;
                    else $url.= "&search=".$request->search;

            }



            // foreach($columns as $cols){
            //     if($cols=="id"){
            //         $list->where($cols,$request->$cols);

            //     }else{
            //         $list->where($cols,'LIKE','%'.$request->$cols.'%');
            //     }

            //     if($url=="") $url = "?".$cols."=".$request->$cols;
            //     else $url.= "&".$cols."=".$request->$cols;
            // }


            $list = $list->paginate(10)->onEachSide(0);

            $list->withPath(url('api/crud/module_category'.$url));

            if($error){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => $error,
                ], 'get data failed', 500);
            }else{
                return ResponseFormatter::success([
                    $list
                ],'successfully get data');
            }


        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function store(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'tanaman_kategori_sub_id' => 'required',
                't_nama' => 'required',
                't_nama_latin' => 'required',
                't_slug' => 'required',
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $cek = Tanaman_kategori_sub::where('id',$request->tanaman_kategori_sub_id)->first();

            if(!$cek){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => "Cant find tanaman_kategori_sub_id on master data",
                ], 'Failed to process data', 500);
            }

            $tanaman_kategori = Tanaman_kategori::where('id',$cek->tanaman_kategori_id)->first();


            $data = Tanaman::create([
                'tanaman_kategori_id'=>$tanaman_kategori->id,
                'tanaman_kategori_nama'=>$tanaman_kategori->tk_nama,
                'tanaman_kategori_sub_id'=>$cek->id,
                'tanaman_kategori_sub_nama'=>$cek->tks_nama,
                't_nama'=>$request->t_nama,
                't_nama_latin'=>$request->t_nama_latin,
                't_slug'=>$request->t_slug,
                'status'=>true
            ]);




            return ResponseFormatter::success([
                $data
            ], 'Success Insert data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function update(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'tanaman_kategori_sub_id' => 'required',
                't_nama' => 'required',
                't_nama_latin' => 'required',
                't_slug' => 'required',
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }


            $cek = Tanaman_kategori_sub::where('id',$request->tanaman_kategori_sub_id)->first();

            if(!$cek){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => "Cant find tanaman_kategori_sub_id on master data",
                ], 'Failed to process data', 500);
            }

            $tanaman_kategori = Tanaman_kategori::where('id',$cek->tanaman_kategori_id)->first();

            $data = Tanaman::where('id', $request->id)->first();

            if(!$data){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => 'cant find data',
                ], 'Failed to process data', 500);
            }


            $data->update([
                'tanaman_kategori_id'=>$tanaman_kategori->id,
                'tanaman_kategori_nama'=>$tanaman_kategori->tk_nama,
                'tanaman_kategori_sub_id'=>$cek->id,
                'tanaman_kategori_sub_nama'=>$cek->tks_nama,
                't_nama'=>$request->t_nama,
                't_nama_latin'=>$request->t_nama_latin,
                't_slug'=>$request->t_slug,
                // 'status'=>true
            ]);

            $response = Tanaman::where('id', $request->id)->first();

            return ResponseFormatter::success([
                $response
            ], 'Success Update data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function delete(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $data = Tanaman::where('id', $request->id)->first();

            if(!$data){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => 'cant find data',
                ], 'Failed to process data', 500);
            }


            $data->update([
                'status'=>false
            ]);

            $response = Tanaman::where('id', $request->id)->first();

            return ResponseFormatter::success([
                $response
            ], 'Success delete data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

}


