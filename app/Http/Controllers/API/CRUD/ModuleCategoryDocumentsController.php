<?php

namespace App\Http\Controllers\API\CRUD;
use App\Models\Modules;
use App\Models\Module_categories;
use App\Models\Module_category_documents;
use App\Http\Controllers\Controller;
use App\Helpers\ResponseFormatter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

// use Illuminate\Support\Facades\Schema;



class ModuleCategoryDocumentsController extends Controller
{
    public function index(Request $request)
    {
        try {
            $req = $request->all();

            $columns = [];
            $error = [];

            // foreach($req as $key => $val){

            //     if (Schema::hasColumn('module', $key)) array_push($columns, $key);
            //     else  array_push($error, "Column ".$key." is exist");

            // }

            $search = $request->search;
            $list = Module_category_documents::with(['module_categories'=>function($s) use ($search){
                if($search){
                    $list->where("mc_nama",'LIKE','%'.$search.'%');

                }
            }])->where('status',true);

            $url = "";

            if($request->id){
                $list->where("id",$request->id);
            }

            if($request->search){
                    // $list->where("id",'LIKE','%'.$request->search.'%');

                    $list->where("mcd_nama",'LIKE','%'.$request->search.'%');
                    if($url=="") $url = "?search=".$request->search;
                    else $url.= "&search=".$request->search;

            }



            // foreach($columns as $cols){
            //     if($cols=="id"){
            //         $list->where($cols,$request->$cols);

            //     }else{
            //         $list->where($cols,'LIKE','%'.$request->$cols.'%');
            //     }

            //     if($url=="") $url = "?".$cols."=".$request->$cols;
            //     else $url.= "&".$cols."=".$request->$cols;
            // }


            $list = $list->paginate(10)->onEachSide(0);

            $list->withPath(url('api/crud/module_category_document'.$url));

            if($error){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => $error,
                ], 'get data failed', 500);
            }else{
                return ResponseFormatter::success([
                    $list
                ],'successfully get data');
            }


        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function store(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'module_categories_id' => 'required',
                'mcd_nama' => 'required',
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $cek = Module_categories::where('id',$request->module_categories_id)->first();
            if(!$cek){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => "Cant find module_categories_id on master data",
                ], 'Failed to process data', 500);
            }




            $data = Module_category_documents::create([
                'module_categories_id'=>$request->module_categories_id,
                'mcd_nama'=>$request->mcd_nama,
                'status'=>true
            ]);



            return ResponseFormatter::success([
                $data
            ], 'Success Insert data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function update(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'module_categories_id' => 'required',
                'mcd_nama' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }


            $cek = Module_categories::where('id',$request->module_categories_id)->first();
            if(!$cek){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => "Cant find module_categories_id on master data",
                ], 'Failed to process data', 500);
            }

            $data = Module_category_documents::where('id', $request->id)->first();

            if(!$data){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => 'cant find data',
                ], 'Failed to process data', 500);
            }


            $data->update([
                'module_categories_id'=>$request->module_categories_id,
                'mcd_nama'=>$request->mcd_nama,
                // 'status'=>true
            ]);

            $response = Module_category_documents::where('id', $request->id)->first();

            return ResponseFormatter::success([
                $response
            ], 'Success Update data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function delete(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $data = Module_category_documents::where('id', $request->id)->first();

            if(!$data){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => 'cant find data',
                ], 'Failed to process data', 500);
            }


            $data->update([
                'status'=>false
            ]);

            $response = Module_category_documents::where('id', $request->id)->first();

            return ResponseFormatter::success([
                $response
            ], 'Success delete data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

}


