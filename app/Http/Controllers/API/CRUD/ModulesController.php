<?php

namespace App\Http\Controllers\API\CRUD;

use App\Models\Modules;
use App\Http\Controllers\Controller;
use App\Helpers\ResponseFormatter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

// use Illuminate\Support\Facades\Schema;



class ModulesController extends Controller
{
    public function index(Request $request)
    {
        try {
            $req = $request->all();

            $columns = [];
            $error = [];

            // foreach($req as $key => $val){

            //     if (Schema::hasColumn('module', $key)) array_push($columns, $key);
            //     else  array_push($error, "Column ".$key." is exist");

            // }


            $list = Modules::where('status',true);
            $url = "";

            if($request->id){
                $list->where("id",$request->id);

            }

            if($request->search){
                    // $list->where("id",'LIKE','%'.$request->search.'%');

                    $list->where("m_nama",'LIKE','%'.$request->search.'%');
                    $list->orWhere("m_ditjen",'LIKE','%'.$request->search.'%');
                    $list->orWhere("nm_kategori_nama",'LIKE','%'.$request->search.'%');

                    if($url=="") $url = "?search=".$request->search;
                    else $url.= "&search=".$request->search;

            }



            // foreach($columns as $cols){
            //     if($cols=="id"){
            //         $list->where($cols,$request->$cols);

            //     }else{
            //         $list->where($cols,'LIKE','%'.$request->$cols.'%');
            //     }

            //     if($url=="") $url = "?".$cols."=".$request->$cols;
            //     else $url.= "&".$cols."=".$request->$cols;
            // }


            $list = $list->paginate(10)->onEachSide(0);

            $list->withPath(url('api/crud/module'.$url));

            if($error){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => $error,
                ], 'get data failed', 500);
            }else{
                return ResponseFormatter::success([
                    $list
                ],'successfully get data');
            }


        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function store(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'm_nama' => 'required',
                'm_ditjen' => 'required',
                'm_kategori' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $data = Modules::create([
                'm_nama'=>$request->m_nama,
                'm_ditjen'=>$request->m_ditjen,
                'm_kategori'=>$request->m_kategori,
                'status'=>true
            ]);



            return ResponseFormatter::success([
                $data
            ], 'Success Insert data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function update(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'm_nama' => 'required',
                'm_ditjen' => 'required',
                'm_kategori' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $data = Modules::where('id', $request->id)->first();

            if(!$data){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => 'cant find data',
                ], 'Failed to process data', 500);
            }


            $data->update([
                'm_nama'=>$request->m_nama,
                'm_ditjen'=>$request->m_ditjen,
                'm_kategori'=>$request->m_kategori,
                // 'status'=>true
            ]);

            $response = Modules::where('id', $request->id)->first();

            return ResponseFormatter::success([
                $response
            ], 'Success Update data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function delete(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $data = Modules::where('id', $request->id)->first();

            if(!$data){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => 'cant find data',
                ], 'Failed to process data', 500);
            }


            $data->update([
                'status'=>false
            ]);

            $response = Modules::where('id', $request->id)->first();

            return ResponseFormatter::success([
                $response
            ], 'Success delete data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

}


