<?php

namespace App\Http\Controllers\API\CRUD;

use App\Models\Pelabuhan;
use App\Http\Controllers\Controller;
use App\Helpers\ResponseFormatter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

// use Illuminate\Support\Facades\Schema;



class PelabuhanController extends Controller
{
    public function index(Request $request)
    {
        try {
            $req = $request->all();

            $columns = [];
            $error = [];

            // foreach($req as $key => $val){

            //     if (Schema::hasColumn('negara', $key)) array_push($columns, $key);
            //     else  array_push($error, "Column ".$key." is exist");

            // }


            $list = Pelabuhan::where('status',true);
            $url = "";

            if($request->id){
                $list->where("id",$request->id);

            }

            if($request->search){
                    // $list->where("id",'LIKE','%'.$request->search.'%');

                    $list->where("pl_nama",'LIKE','%'.$request->search.'%');
                    $list->orWhere("pl_code_nama",'LIKE','%'.$request->search.'%');
                    $list->orWhere("pl_code",'LIKE','%'.$request->search.'%');
                    $list->orWhere("pl_lokasi",'LIKE','%'.$request->search.'%');
                    $list->orWhere("pl_ncode",'LIKE','%'.$request->search.'%');
                    $list->orWhere("pl_ps_code",'LIKE','%'.$request->search.'%');

                    if($url=="") $url = "?search=".$request->search;
                    else $url.= "&search=".$request->search;

            }



            // foreach($columns as $cols){
            //     if($cols=="id"){
            //         $list->where($cols,$request->$cols);

            //     }else{
            //         $list->where($cols,'LIKE','%'.$request->$cols.'%');
            //     }

            //     if($url=="") $url = "?".$cols."=".$request->$cols;
            //     else $url.= "&".$cols."=".$request->$cols;
            // }


            $list = $list->paginate(10)->onEachSide(0);

            $list->withPath(url('api/crud/pelabuhan'.$url));

            if($error){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => $error,
                ], 'get data failed', 500);
            }else{
                return ResponseFormatter::success([
                    $list
                ],'successfully get data');
            }


        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function store(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'pl_nama' => 'required',
                'pl_code_nama' => 'required',
                'pl_code' => 'required',
                'pl_lokasi' => 'required',
                'pl_ncode' => 'required',
                'pl_ps_code' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $data = Pelabuhan::create([
                'pl_nama'=>$request->pl_nama,
                'pl_code_nama'=>$request->pl_code_nama,
                'pl_code'=>$request->pl_code,
                'pl_lokasi'=>$request->pl_lokasi,
                'pl_ncode'=>$request->pl_ncode,
                'pl_ps_code'=>$request->pl_ps_code,
                'status'=>true
            ]);



            return ResponseFormatter::success([
                $data
            ], 'Success Insert data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function update(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'pl_nama' => 'required',
                'pl_code_nama' => 'required',
                'pl_code' => 'required',
                'pl_lokasi' => 'required',
                'pl_ncode' => 'required',
                'pl_ps_code' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $data = Pelabuhan::where('id', $request->id)->first();

            if(!$data){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => 'cant find data',
                ], 'Failed to process data', 500);
            }


            $data->update([
                'pl_nama'=>$request->pl_nama,
                'pl_code_nama'=>$request->pl_code_nama,
                'pl_code'=>$request->pl_code,
                'pl_lokasi'=>$request->pl_lokasi,
                'pl_ncode'=>$request->pl_ncode,
                'pl_ps_code'=>$request->pl_ps_code,
                // 'status'=>true
            ]);

            $response = Pelabuhan::where('id', $request->id)->first();

            return ResponseFormatter::success([
                $response
            ], 'Success Update data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

    public function delete(Request $request)
    {
        try{

            $validator = Validator::make($request->all(), [
                'id' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $data = Pelabuhan::where('id', $request->id)->first();

            if(!$data){
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => 'cant find data',
                ], 'Failed to process data', 500);
            }


            $data->update([
                'status'=>false
            ]);

            $response = Pelabuhan::where('id', $request->id)->first();

            return ResponseFormatter::success([
                $response
            ], 'Success delete data');

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Failed to process data', 500);
        }

    }

}


