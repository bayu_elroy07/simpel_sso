<?php

namespace App\Http\Controllers\API;
use App\Models\Modules;
use App\Models\Tanaman_kategori;
use App\Models\Negara;
use App\Models\Tujuan_penggunaan_benih;
use App\Models\Pelabuhan;
use App\Models\Tipe_pemohon;
use App\Models\Kbli_perusahaan;
use App\Models\Jenis_perusahaan;
use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\Kecamatan;
use App\Models\Kelurahan;

use App\Http\Controllers\Controller;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;


class MasterController extends Controller
{
    public function category(Request $request)
    {
        $module = Modules::select('m_kategori')->distinct()->get();
        $jsonmodule = [];
        foreach($module as $m){
            $jsonmodule[] = $m->m_kategori;
        }
        return ResponseFormatter::success([
            'master' => $jsonmodule
        ],'successfully get data');
    }



    //
    public function modules(Request $request)
    {
        $kategori = $request->kategori;
        $module_categories_id = $request->module_categories_id;
        $modules_id = $request->modules_id;
        $module = Modules::with(['module_categories'=>function($s) use ($module_categories_id){
            if($module_categories_id){
                $s->where('id',$module_categories_id);
            }
            $s->where('status',true);

        },'module_categories.module_category_documents'=>function($s){
                $s->where('status',true);
        }]);

        $module->whereHas('module_categories',function($s) use ($module_categories_id){
            if($module_categories_id){
                $s->where('id',$module_categories_id);
            }
        });

        if($kategori!=""){
            $module->where('m_kategori',$kategori);
        }

        if($modules_id!=""){
            $module->where('id',$modules_id);
        }

        $module->where('status',true);


        $module = $module->get();

        return ResponseFormatter::success([
            'master' => $module
        ],'successfully get data');


    }

    public function tanaman(Request $request)
    {
        $tanaman_kategori_id = $request->tanaman_kategori_id;
        $tanaman_kategori_sub_id = $request->tanaman_kategori_sub_id;
        $tanaman_id = $request->tanaman_id;

        $tanaman = Tanaman_kategori::with(['tanaman_kategori_sub'=>function($s) use ($tanaman_kategori_sub_id){
            if($tanaman_kategori_sub_id){
                $s->where('id',$tanaman_kategori_sub_id);
            }
            $s->where('status',true);

        },'tanaman_kategori_sub.tanaman'=>function($s) use ($tanaman_id){
            if($tanaman_id){
                $s->where('id',$tanaman_id);
            }
            $s->where('status',true);

        }]);



        $tanaman->whereHas('tanaman_kategori_sub',function($s) use ($tanaman_kategori_sub_id){
            if($tanaman_kategori_sub_id){
                $s->where('id',$tanaman_kategori_sub_id);
            }
        });


        $tanaman->whereHas('tanaman_kategori_sub.tanaman',function($s) use ($tanaman_id){
            if($tanaman_id){
                $s->where('id',$tanaman_id);
            }
        });



        if($tanaman_kategori_id!=""){
            $module->where('id',$tanaman_kategori_id);
        }

        $tanaman->where('status',true);
        $tanaman = $tanaman->get();

        return ResponseFormatter::success([
            'master' => $tanaman
        ],'successfully get data');

    }

    public function pelabuhan(Request $request)
    {
        $id = $request->id;
        $master = Pelabuhan::where('status',true);
        if($id){
            $master->where('id',$id);
        }
        $json = $master->get();
        return ResponseFormatter::success([
            'master' => $json
        ],'successfully get data');
    }

    public function negara(Request $request)
    {
        $id = $request->id;
        $master = Negara::where('status',true);
        if($id){
            $master->where('id',$id);
        }
        $json = $master->get();
        return ResponseFormatter::success([
            'master' => $json
        ],'successfully get data');

    }

    public function tujuan_penggunaan_benih(Request $request)
    {
        $id = $request->id;
        $master = Tujuan_penggunaan_benih::where('status',true);
        if($id){
            $master->where('id',$id);
        }
        $json = $master->get();
        return ResponseFormatter::success([
            'master' => $json
        ],'successfully get data');
    }

    public function tipe_pemohon(Request $request)
    {
        $id = $request->id;
        $master = Tipe_pemohon::with(['dokumen_pemohon'=>function($s){

            $s->where('status',true);

        }])->where('status',true);

        if($id){
            $master->where('id',$id);
        }
        $json = $master->get();
        return ResponseFormatter::success([
            'master' => $json
        ],'successfully get data');
    }

    public function jenis_perusahaan(Request $request)
    {
        $id = $request->id;
        $master = Jenis_perusahaan::where('status',true);

        if($id){
            $master->where('id',$id);
        }
        $json = $master->get();
        return ResponseFormatter::success([
            'master' => $json
        ],'successfully get data');
    }

    public function kbli_perusahaan(Request $request)
    {
        $id = $request->id;
        $master = Kbli_perusahaan::where('status',true);

        if($id){
            $master->where('id',$id);
        }
        $json = $master->get();
        return ResponseFormatter::success([
            'master' => $json
        ],'successfully get data');
    }

    public function daerah(Request $request)
    {


        $type = $request->type;
        $id = $request->id;
        $parent_id = $request->parent_id;

        $validator = Validator::make($request->all(), [
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponseFormatter::error(
                [
                    'error' => $validator->errors()
                ],
                'Pasitikan data terisi',
                401
            );
        }

        if($id){
            if($id==""){
                return ResponseFormatter::error(
                    [
                        'error' => ["id"=>'ID tidak boleh kosong']
                    ],
                    'Pasitikan data ID terisi',
                    401
                );
            }
        }

        if($parent_id){
            if($parent_id==""){
                return ResponseFormatter::error(
                    [
                        'error' => ["parent_id"=>'ID tidak boleh kosong']
                    ],
                    'Pasitikan data Parent ID terisi',
                    401
                );
            }
        }

        if($type=="provinsi"){
            $json = Provinsi::where('status',true);
            if($id){
                $json = $json->where('id',$id);
            }
        }else if($type=="kota"){
            $json = Kota::where('status',true);
            if($id){
                $json = $json->where('id',$id);
            }

            if($parent_id){
                $json = $json->where('provinsi_id',$parent_id);
            }

        }else if($type=="kecamatan"){
            $json = Kecamatan::where('status',true);
            if($id){
                $json = $json->where('id',$id);
            }

            if($parent_id){
                $json = $json->where('kota_id',$parent_id);
            }

        }else if($type=="kelurahan"){
            $json = Kelurahan::where('status',true);
            if($id){
                $json = $json->where('id',$id);
            }

            if($parent_id){
                $json = $json->where('kecamatan_id',$parent_id);
            }
        }else if($type=="tracking"){
            if($parent_id==""){
                return ResponseFormatter::error(
                    [
                        'error' => ["parent_id"=>'Parent ID tidak boleh kosong']
                    ],
                    'Pasitikan data Parent ID terisi dengan id kelurahan',
                    401
                );
            }else{
                $json = Kelurahan::where('status',true);
                $json = $json->where('id',$parent_id);

            }
        }

        if($type!="tracking"){
            $json = $json->get();
        }else{
            $json = $json->first();
            $json->kecamatan;
            $json->kecamatan->kota;
            $json->kecamatan->kota->provinsi;

        }

        // $json = Provinsi::with(['kota'=>function($s) use ($kelurahan_id){
        //     if($kelurahan_id){
        //         $s->where('id',$kelurahan_id);
        //     }
        //     $s->where('status',true);

        // }])->get();

        return ResponseFormatter::success([
            'master' => $json
        ],'successfully get data');

    }







}
