<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Mail\SendMail;
use App\Mail\SendMailRegistered;
use App\Actions\Fortify\PasswordValidationRules;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Perusahaan;
use App\Models\Kelurahan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

use Exception;


class UserController extends Controller
{
    use PasswordValidationRules;

    /**
     * @param Request $request
     * @return mixed
     */
    public function fetch(Request $request)
    {

        $json = $request->user();
        $perusahaan = Perusahaan::where('nib',$request->user()->nib)->first();
        if($perusahaan){

            $json['perusahaan'] = json_decode($perusahaan->p_json,true);
        }else{
            $json['perusahaan'] = [];
        }

        return ResponseFormatter::success(

            $json

            ,'Data profile user berhasil diambil');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    [
                        'error' => $validator->errors()
                    ],
                    'Pasitikan data terisi',
                    401
                );
            }

            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials)) {
                return ResponseFormatter::error([
                    'message' => 'Unauthorized',
                    'error' => 'Account cant find'

                ], 'Authentication Failed', 500);
            }

            $user = User::where('email', $request->email)->first();
            if (!Hash::check($request->password, $user->password, [])) {
                throw new \Exception('Invalid Credentials');
            }

            $previous_session = $user->session_id;
            if ($previous_session) {
                $personalaccess = DB::table('personal_access_tokens')->where('tokenable_id',$user->id)->count();
                if($personalaccess==0){
                    Session::getHandler()->destroy($previous_session);
                    $user->tokens()->delete();
                    Auth::user()->session_id = Session::getId();
                     Auth::user()->save();
                }else{
                    return ResponseFormatter::error([
                        'message' => 'Unauthorized',
                        'error' => 'Session has been used',
                    ], 'Authentication Failed', 500);
                }

            }else{
                Auth::user()->session_id = Session::getId();
                Auth::user()->save();
            }


            $tokenResult = $user->createToken('authToken')->plainTextToken;


            $users = User::where('email', $request->email)->first();
            return ResponseFormatter::success([
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $users
            ], 'Authenticated');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Authentication Failed', 500);
        }
    }

/**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'unique:users', 'max:255'],
                'nip' => ['required', 'string', 'unique:users'],
                'nib' => [ 'string', 'size:13','unique:users'],
                'phone' => ['required', 'string'],
                'npwp' => ['required', 'string','size:15'],
                'tipe_pemohon_id'=>['required'],
                'password' => $this->passwordRules()
            ]);
            if ($validator->fails()) {
                return ResponseFormatter::error(
                    ['error' => $validator->errors()],
                    'Pasitikan data terisi',
                    401
                );
            }

            User::create([
                'name'  => $request->name,
                'email' => $request->email,
                'nip' => $request->nip,
                'password' => Hash::make($request->password),
                'nib' => $request->nib,
                'phone' => $request->phone,
                'npwp' => $request->npwp,
                'roles' => 'PEMOHON',
                'tipe_pemohon_id'=> $request->tipe_pemohon_id
            ]);



            $perusahaan = Perusahaan::where('nib',$request->nib)->first();
            if($perusahaan){
                $perusahaan->update([
                    'p_nama'=>$request->p_nama,
                    'p_npwp'=>$request->p_npwp,
                    'p_penanggung_jawab'=>$request->p_penanggung_jawab,
                    'p_email'=>$request->p_email,
                    'p_telp'=>$request->p_telp,
                    'p_alamat'=>$request->p_alamat,
                    'jenis_perusahaan_id'=>$request->jenis_perusahaan_id,
                    'kbli_perusahaan_id'=>$request->kbli_perusahaan_id,
                    'p_hp'=>$request->p_hp,
                    'p_fax'=>$request->p_fax,
                    'provinsi_id'=>$request->provinsi_id,
                    'kota_id'=>$request->kota_id,
                    'kecamatan_id'=>$request->kecamatan_id,
                    'kelurahan_id'=>$request->kelurahan_id,
                    'p_kode_pos'=>$request->p_kode_pos
                ]);
            }




            $user = User::where('email', $request->email)->first();
            $tokenResult = $user->createToken('authToken')->plainTextToken;
            $this->sendEmailRegistered($request->email);

            $user->session_id = Session::getId();
            $user->save();

            return ResponseFormatter::success([
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user
            ], 'User Registered');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Authentication Failed', 500);
        }
    }


    public function sendEmailRegistered($email){

        $token = sha1(md5($email));
        $response = Mail::to($email)->send(new SendMailRegistered($token));
        if (Mail::failures()) {
            return false;
        }else{
            $user = User::where('email',$email)->first();
            $user->update([
                'token_email'=>$token
            ]);
            return true;
        }
    }

     public function logout(Request $request)
    {
        $previous_session = $request->user()->session_id;

        Session::getHandler()->destroy($previous_session);
        $request->user()->update([
            'session_id'=>''
        ]);
        $token = $request->user()->currentAccessToken()->delete();
        return ResponseFormatter::success($token,'Token Revoked');
    }

    public function updateProfile(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        $user->update($data);

        $password = ['password' => Hash::make($request->password)];
        $user->update($password);

        return ResponseFormatter::success($user,'Profile Updated');
    }

    public function forgot_password(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'email' => ['required', 'string', 'email', 'max:255'],
            ]);
            if ($validator->fails()) {
                return ResponseFormatter::error(
                    ['error' => $validator->errors()],
                    'Pasitikan data terisi',
                    401
                );
            }

            $user = User::where('email', $request->email)->first();

            if($user){
                $isToken = DB::table('password_resets')->where('email', $request->email)->first();
                if($isToken) {
                    $created_at = $isToken->created_at;
                    $time = Carbon::parse($created_at);
                    $endTime = $time->addMinutes(5);
                    if(Carbon::now()<=$endTime){
                        return ResponseFormatter::error([
                            'error' => 'an Email has been sent'
                        ], 'Token is registered',400);
                    }else{
                        $response = $this->sendEmail($request->email);
                        if($response){
                            return ResponseFormatter::success([
                                            'user' => $user
                                        ], 'Success send emails');
                        }else{
                            return ResponseFormatter::error([
                                'user' => $user
                            ], 'Failed send emails',400);
                        }
                    }
                }else{
                       $response = $this->sendEmail($request->email);
                        if($response){
                            return ResponseFormatter::success([
                                            'user' => $user
                                        ], 'Success send email');
                        }else{
                            return ResponseFormatter::error([
                                'user' => $user
                            ], 'Failed send email',400);
                        }
                }


            }else{
                return ResponseFormatter::error([
                    'message' => 'Something went wrong',
                    'error' => $error,
                ], 'Authentication Failed', 500);
            }



        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Authentication Failed', 500);
        }
    }

    public function sendEmail($email){

        $token = $this->createToken($email);
        $response = Mail::to($email)->send(new SendMail($token));
        if (Mail::failures()) {
            // return response showing failed emails
            return false;
        }else{
            return true;
        }
    }

    function generateNumericOTP($n) {

        $generator = "1357902468";
        $result = "";
        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand()%(strlen($generator))), 1);
        }

        // Return result
        return $result;
    }

    public function createToken($email){
        $isToken = DB::table('password_resets')->where('email', $email)->first();

        if($isToken) {
          return $isToken->token;
        }

        $token = $this->generateNumericOTP(5);
        $this->saveToken($token, $email);
        return $token;
      }

      public function saveToken($token, $email){
          DB::table('password_resets')->insert([
              'email' => $email,
              'token' => $token,
              'created_at' => Carbon::now()
          ]);
      }



    public function update_password(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => ['required', 'string', 'email', 'max:255'],
                'token' => ['required', 'string'],
                'password' => $this->passwordRules()
            ]);

            if ($validator->fails()) {
                return ResponseFormatter::error(
                    ['error' => $validator->errors()],
                    'Pasitikan data terisi',
                    401
                );
            }

            $isToken = DB::table('password_resets')->where('email',$request->email)->where('token',$request->token)->first();
            if($isToken){
                $created_at = $isToken->created_at;
                $time = Carbon::parse($created_at);
                $endTime = $time->addMinutes(5);
                if(Carbon::now()<=$endTime){
                    $user = User::where('email',$request->email)->first();
                    $user->update([
                        'password'=>Hash::make($request->password)
                    ]);

                    $this->validateToken($request)->delete();

                    return ResponseFormatter::success([
                        'user' => $user
                    ], 'Password success updated');
                }else{
                    return ResponseFormatter::error([
                        'message' => 'Something went wrong , token is expired',
                    ], 'Authentication Failed', 500);
                }


            }else{
                return ResponseFormatter::error([
                    'message' => 'Something went wrong , token is not registered',
                ], 'Authentication Failed', 500);
            }


        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'Authentication Failed', 500);
        }
    }

    private function validateToken($request){
        return DB::table('password_resets')->where([
            'email' => $request->email,
            'token' => $request->token
        ]);
    }


    public function checkPerusahaan(Request $request)
    {
        # code...
        $json = $this->getPerusahaan($request);
        try {
            $nib = $request->nib;
            $json = $this->getPerusahaan($request);
            if(is_array($json)){
                if($json['status']==true){
                    return ResponseFormatter::success([
                        $json
                    ],'successfully get data');
                }else{
                    return ResponseFormatter::error([
                        'message' => $json['message'],
                        'error' => 'NIB cant find',
                    ], 'Authentication Failed', 500);

                }
            }else{
                    return ResponseFormatter::error([
                        'message' => 'Something went wrong',
                        'error' => 'NIB cant find',
                    ], 'cant get data fron Server OSS Pertanian', 500);
            }
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'cant get data fron Server OSS Pertanian', 500);
        }
    }

    private function url_get_contents ($Url) {
        if (!function_exists('curl_init')){
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    private function getPerusahaan($request)
    {
        # code...
        try {

            $nib = $request->nib;
            $username = "simpel";
            $password = "simpel123";
            $token = SHA1($username.$password.date('Ymd'));
            $section = $this->url_get_contents('http://izinusaha.pertanian.go.id/midoss/services/get_inqueryNIB?nib='.$nib.'&username=simpel&password=simpel123&token='.$token);
            $data = json_decode($section,true);

            if($data['responinqueryNIB']['kode']!=200){
                $json = [
                    'status'=>false,
                    'data'=>[]
                ];
            }

            $perusahaanData = $data['responinqueryNIB']['dataNIB'];

            $penanggungjawab = [];
            $i=1;
            foreach($perusahaanData['penanggung_jwb'] as $pj){
                // if($pj['jabatan_penanggung_jwb']=="DIREKTUR"){
                if($i==1){
                    $penanggungjawab = $pj;
                }
                // }
                $i++;
            }
            // $penanggungjawab = in_array('DIREKTUR',$perusahaanData['penanggung_jwb']);

            $perusahaan['nib'] = $perusahaanData['nib'];
            $perusahaan['p_nama'] = $perusahaanData['nama_perseroan'];
            $perusahaan['p_npwp'] = $perusahaanData['npwp_perseroan'];
            $perusahaan['p_email'] = $perusahaanData['email_perusahaan'];
            $perusahaan['p_telp'] = $perusahaanData['nomor_telpon_perseroan'];
            $perusahaan['p_alamat'] = $perusahaanData['alamat_perseroan'];
            $perusahaan['p_fax'] = "";
            $perusahaan['p_hp'] = "";
            $perusahaan['p_kode_pos'] = $perusahaanData['kode_pos_perseroan'];
            if($penanggungjawab){
                $perusahaan['p_penanggung_jawab'] = $penanggungjawab['nama_penanggung_jwb'];
            }
            $perusahaan['p_json'] = $section;
            $kelurahan = Kelurahan::with('kecamatan')->where('id',$perusahaanData['perseroan_daerah_id'])->first();
            $perusahaan['provinsi_id'] = $kelurahan->kecamatan->kota->provinsi->id;
            $perusahaan['kota_id'] = $kelurahan->kecamatan->kota->id;
            $perusahaan['kecamatan_id'] = $kelurahan->kecamatan->id;
            $perusahaan['kelurahan_id'] = $kelurahan->id;



            $cek = Perusahaan::where('nib',$perusahaanData['nib'])->first();
            if(!$cek){
                Perusahaan::create($perusahaan);
            }else{
                $cek->update($perusahaan);
            }

            return $json = [
                                'status'=>true,
                                'message'=>$data['responinqueryNIB']['keterangan'],
                                'data'=>$perusahaan
            ];

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ], 'cant get data fron Server OSS Pertanian', 500);
        }



    }





}
