<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function email_verify(Request $request)
    {

        $user = User::where('token_email',$request->token)->first();
        $date = (Carbon::now()->format('Y-m-d H:i:s'));

        if($user){
            $user->update([
                'token_email'=>'',
                'email_verified_at'=>$date
            ]);
            $data['status'] = "Verified email - success";
            $data['text'] = "Email : ".$user->email." was successfully verified";
        }else{
            $data['status'] = "Verified email - failed";
            $data['text'] = "Email failed to verify , expired token";
        }
        return view('Email/email-registered',$data);
    }
}
