<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\User;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\Auth;


class CheckEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->email_verified_at == NULL) {
            // return $next($request);
            return ResponseFormatter::error([
                'message' => 'email has not been verified, check inbox email for verified'
            ], 'Authentication Failed', 500);
        }
        return $next($request);
    }
}
