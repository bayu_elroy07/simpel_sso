<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use App\Helpers\ResponseFormatter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class OneTimeSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = User::where('id',Auth::user()->id)->first();

        if (Auth::user()->session_id != $user->session_id) {
            $token = Auth::user()->currentAccessToken()->delete();
            return ResponseFormatter::error([
                'message' => 'Session is not valid , make sure account in other device sign out.'
            ], 'Authentication Failed', 500);
        }

        return $next($request);
    }
}
