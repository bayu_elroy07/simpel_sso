<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\MasterController;

// CRUD

use App\Http\Controllers\API\CRUD\NegaraController;
use App\Http\Controllers\API\CRUD\ModulesController;
use App\Http\Controllers\API\CRUD\ModuleCategoriesController;
use App\Http\Controllers\API\CRUD\ModuleCategoryDocumentsController;
use App\Http\Controllers\API\CRUD\TipePemohonController;
use App\Http\Controllers\API\CRUD\TujuanPenggunaanBenihController;
use App\Http\Controllers\API\CRUD\PelabuhanController;
use App\Http\Controllers\API\CRUD\TanamanKategoriController;
use App\Http\Controllers\API\CRUD\TanamanKategoriSubController;
use App\Http\Controllers\API\CRUD\TanamanController;
use App\Http\Controllers\API\CRUD\DokumenPemohonController;
use App\Http\Controllers\API\CRUD\RoleController;

use Auth\VerificationController;
use App\Helpers\ResponseFormatter;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', [UserController::class, 'login']);
Route::post('register', [UserController::class, 'register']);
Route::post('forgot-password', [UserController::class, 'forgot_password']);
Route::post('update-password', [UserController::class, 'update_password']);

Route::get('failed', function () {
    return ResponseFormatter::error([
        'message' => 'Cant find token authentication'
    ], 'Authentication Failed', 500);
})->name('failed');


Route::post('failed', [UserController::class, 'update_password']);
Route::get('getperusahaan', [UserController::class, 'checkPerusahaan']);


Route::prefix('master')->group(function () {

Route::get('daerah', [MasterController::class, 'daerah']);
Route::get('tipe_pemohon', [MasterController::class, 'tipe_pemohon']);
Route::get('jenis_perusahaan', [MasterController::class, 'jenis_perusahaan']);
Route::get('kbli_perusahaan', [MasterController::class, 'kbli_perusahaan']);
// Route::get('daerah', [UserController::class, 'checkPerusahaan']);
});



Route::middleware(['auth:sanctum','checkemail','onetimesession'])->group(function () {
        Route::get('user', [UserController::class, 'fetch']);
        Route::post('user', [UserController::class, 'updateProfile']);
        Route::post('logout', [UserController::class, 'logout']);
        //master
        Route::prefix('master')->group(function () {
            Route::get('module', [MasterController::class, 'modules']);
            Route::get('category', [MasterController::class, 'category']);
            Route::get('tanaman', [MasterController::class, 'tanaman']);
            Route::get('pelabuhan', [MasterController::class, 'pelabuhan']);
            Route::get('negara', [MasterController::class, 'negara']);
            Route::get('tujuan_penggunaan_benih', [MasterController::class, 'tujuan_penggunaan_benih']);
        });

        Route::prefix('crud')->group(function () {
            // Master

            Route::prefix('negara')->group(function () {
                Route::get('/', [NegaraController::class, 'index']);
                Route::post('/', [NegaraController::class, 'store']);
                Route::put('/', [NegaraController::class, 'update']);
                Route::delete('/', [NegaraController::class, 'delete']);
            });

            Route::prefix('module')->group(function () {
                Route::get('/', [ModulesController::class, 'index']);
                Route::post('/', [ModulesController::class, 'store']);
                Route::put('/', [ModulesController::class, 'update']);
                Route::delete('/', [ModulesController::class, 'delete']);
            });


            Route::prefix('module_category')->group(function () {
                Route::get('/', [ModuleCategoriesController::class, 'index']);
                Route::post('/', [ModuleCategoriesController::class, 'store']);
                Route::put('/', [ModuleCategoriesController::class, 'update']);
                Route::delete('/', [ModuleCategoriesController::class, 'delete']);
            });

            Route::prefix('module_category_document')->group(function () {
                Route::get('/', [ModuleCategoryDocumentsController::class, 'index']);
                Route::post('/', [ModuleCategoryDocumentsController::class, 'store']);
                Route::put('/', [ModuleCategoryDocumentsController::class, 'update']);
                Route::delete('/', [ModuleCategoryDocumentsController::class, 'delete']);
            });

            Route::prefix('tipe_pemohon')->group(function () {
                Route::get('/', [TipePemohonController::class, 'index']);
                Route::post('/', [TipePemohonController::class, 'store']);
                Route::put('/', [TipePemohonController::class, 'update']);
                Route::delete('/', [TipePemohonController::class, 'delete']);
            });

            Route::prefix('tujuan_penggunaan_benih')->group(function () {
                Route::get('/', [TujuanPenggunaanBenihController::class, 'index']);
                Route::post('/', [TujuanPenggunaanBenihController::class, 'store']);
                Route::put('/', [TujuanPenggunaanBenihController::class, 'update']);
                Route::delete('/', [TujuanPenggunaanBenihController::class, 'delete']);
            });

            Route::prefix('pelabuhan')->group(function () {
                Route::get('/', [PelabuhanController::class, 'index']);
                Route::post('/', [PelabuhanController::class, 'store']);
                Route::put('/', [PelabuhanController::class, 'update']);
                Route::delete('/', [PelabuhanController::class, 'delete']);
            });


            Route::prefix('tanaman_kategori')->group(function () {
                Route::get('/', [TanamanKategoriController::class, 'index']);
                Route::post('/', [TanamanKategoriController::class, 'store']);
                Route::put('/', [TanamanKategoriController::class, 'update']);
                Route::delete('/', [TanamanKategoriController::class, 'delete']);
            });

            Route::prefix('tanaman_kategori_sub')->group(function () {
                Route::get('/', [TanamanKategoriSubController::class, 'index']);
                Route::post('/', [TanamanKategoriSubController::class, 'store']);
                Route::put('/', [TanamanKategoriSubController::class, 'update']);
                Route::delete('/', [TanamanKategoriSubController::class, 'delete']);
            });

            Route::prefix('tanaman')->group(function () {
                Route::get('/', [TanamanController::class, 'index']);
                Route::post('/', [TanamanController::class, 'store']);
                Route::put('/', [TanamanController::class, 'update']);
                Route::delete('/', [TanamanController::class, 'delete']);
            });

            Route::prefix('dokumen_pemohon')->group(function () {
                Route::get('/', [DokumenPemohonController::class, 'index']);
                Route::post('/', [DokumenPemohonController::class, 'store']);
                Route::put('/', [DokumenPemohonController::class, 'update']);
                Route::delete('/', [DokumenPemohonController::class, 'delete']);
            });

            Route::prefix('role')->group(function () {
                Route::get('/', [RoleController::class, 'index']);
                Route::post('/', [RoleController::class, 'store']);
                Route::put('/', [RoleController::class, 'update']);
                Route::delete('/', [RoleController::class, 'delete']);
            });
    });



});
