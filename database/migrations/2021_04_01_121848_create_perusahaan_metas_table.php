<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerusahaanMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perusahaan_meta', function (Blueprint $table) {
            $table->id();
            $table->string('nib')->unique()->index();
            // $table->foreign('nib')->references('nib')->on('perusahaan');
            $table->string('pm_slug')->nullable();
            $table->longText('pm_konten')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perusahaan_meta');
    }
}
