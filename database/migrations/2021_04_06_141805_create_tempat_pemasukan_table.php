<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempatPemasukanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelabuhan', function (Blueprint $table) {
            $table->id();
            $table->string('pl_nama')->nullable();
            $table->string('pl_code_nama')->nullable();
            $table->string('pl_code')->nullable();
            $table->string('pl_lokasi')->nullable();
            $table->string('pl_ncode')->nullable();
            $table->string('pl_ps_code')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelabuhan');
    }
}
