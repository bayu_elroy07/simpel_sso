<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPerusahaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('perusahaan', function (Blueprint $table) {
            $table->integer('jenis_perusahaan_id')->index();
            $table->integer('kbli_perusahaan_id')->index();
            $table->string('p_hp');
            $table->string('p_fax');
            $table->bigInteger('provinsi_id')->index();
            $table->bigInteger('kota_id')->index();
            $table->bigInteger('kecamatan_id')->index();
            $table->bigInteger('kelurahan_id')->index();
            $table->string('p_kode_pos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('perusahaan', function (Blueprint $table) {
            $table->dropColumn(
                'jenis_perusahaan_id',
                'kbli_perusahaan_id',
                'p_hp',
                'p_fax',
                'provinsi_id',
                'kota_id',
                'kecamatan_id',
                'kelurahan_id',
                'p_kode_pos'
            );
        });
    }
}
