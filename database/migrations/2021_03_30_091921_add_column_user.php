<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('nip')
                    ->after('email')
                    ->nullable()
                    ->unique()
                    ->index();

            $table->text('phone')
                    ->after('email')
                    ->nullable();

            // $table->text('nib')
            //         ->after('phone')
            //         ->index()
            //         ->nullable();

            $table->text('npwp')
                    ->after('nib_email')
                    ->nullable();

            $table->string('roles')
                    ->after('npwp')
                    ->default('USER');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('nip','phone', 'npwp', 'roles');
        });

    }
}
