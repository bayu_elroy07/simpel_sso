<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleCategoryDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_category_documents', function (Blueprint $table) {
            $table->id();
            $table->integer('module_categories_id')->index();
            // $table->foreign('module_categories_id')->references('id')->on('module_categories');
            $table->string('mcd_nama')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_category_documents');
    }
}
