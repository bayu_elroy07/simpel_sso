<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerusahaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perusahaan', function (Blueprint $table) {
            $table->id();
            $table->integer('nib');
            $table->string('p_nama')->nullable();
            $table->string('p_npwp')->unique()->nullable();
            $table->string('p_penanggung_jawab')->nullable();
            $table->string('p_email')->unique()->nullable();
            $table->string('p_telp')->nullable();
            $table->string('p_alamat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perusahaan');
    }
}
