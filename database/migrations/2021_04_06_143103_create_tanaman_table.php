<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTanamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanaman', function (Blueprint $table) {
            $table->id();
            $table->integer('tanaman_kategori_id')->index();
            // $table->foreign('tanaman_kategori_id')->references('id')->on('tanaman_kategori');
            $table->string('tanaman_kategori_nama')->nullable();
            $table->integer('tanaman_kategori_sub_id')->index();
            // $table->foreign('tanaman_kategori_sub_id')->references('id')->on('tanaman_kategori_sub');
            $table->string('tanaman_kategori_sub_nama')->nullable();
            $table->string('mj_nama')->nullable();
            $table->string('mj_nama_latin')->nullable();
            $table->string('mj_slug')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanaman');
    }
}
