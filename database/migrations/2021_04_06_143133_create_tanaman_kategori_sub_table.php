<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTanamanKategoriSubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanaman_kategori_sub', function (Blueprint $table) {
            $table->id();
            $table->integer('tanaman_kategori_id')->index();
            // $table->foreign('tanaman_kategori_id')->references('id')->on('tanaman_kategori');
            $table->string('tanaman_kategori_nama')->nullable();
            $table->string('tks_nama')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanaman_kategori_sub');
    }
}
