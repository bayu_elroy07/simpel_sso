<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ModuleCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('module_categoryes')->insert(
            [
                'module_id' => 1,
                'mc_nama'   => 'Pemasukan Benih Perkebunan',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 1,
                'mc_nama'   => 'Pemasukan Benih Tanaman Semusim',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 1,
                'mc_nama'   => 'Pemasukan Benih Tanaman Rempah',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 1,
                'mc_nama'   => 'Pemasukan Benih Kelapa Sawit',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 1,
                'mc_nama'   => 'Laporan Realisasi',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 1,
                'mc_nama'   => 'Pemasukan Benih Tanaman Penyegar',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 1,
                'mc_nama'   => 'Pemasukan Benih Kelapa Sawit (2018)',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 2,
                'mc_nama'   => 'Pengeluaran Benih Tanaman Tahunan',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 2,
                'mc_nama'   => 'Pengeluaran Benih Tanaman Semusim',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 2,
                'mc_nama'   => 'Pengeluaran Benih Tanaman Rempah',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 2,
                'mc_nama'   => 'Pengeluaran Benih Kelapa Sawit',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 2,
                'mc_nama'   => 'Laporan Realisasi',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 2,
                'mc_nama'   => 'Pengeluaran Benih Tanaman Penyegar',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'module_id' => 2,
                'mc_nama'   => 'Pengeluaran Benih Kelapa Sawit (2018)',
                'status'    => true,
                'created_at' => date('Y-m-d H:i:s')
            ]
        );
    }
}
