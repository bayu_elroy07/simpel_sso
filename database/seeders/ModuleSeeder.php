<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert(
            [
                'id'    => 1,
                'm_nama' => 'Pemasukan Benih Perkebunan',
                'm_kategori' => 'Perkebunan',
                'm_ditjen' => 'bun',
                'status' => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'    => 2,
                'm_nama' => 'Pengeluaran Benih Perkebunan',
                'm_kategori' => 'Perkebunan',
                'm_ditjen' => 'bun',
                'status' => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'    => 3,
                'm_nama' => 'Pemasukan Benih Hortikultura',
                'm_kategori' => 'Hortikultura',
                'm_ditjen' => 'hortikultura',
                'status' => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'    => 4,
                'm_nama' => 'Pengeluaran Benih Hortikultura',
                'm_kategori' => 'Hortikultura',
                'm_ditjen' => 'hortikultura',
                'status' => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'    => 5,
                'm_nama' => 'Pemasukan Benih Pangan',
                'm_kategori' => 'Tanaman Pangan',
                'm_ditjen' => 'pangan',
                'status' => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'    => 6,
                'm_nama' => 'Pengeluaran Benih Pangan',
                'm_kategori' => 'Tanaman Pangan',
                'm_ditjen' => 'pangan',
                'status' => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'    => 7,
                'm_nama' => 'Pemasukan Sumber Daya Genetik',
                'm_kategori' => 'Sumber Daya Genetik',
                'm_ditjen' => 'litbang',
                'status' => true,
                'created_at' => date('Y-m-d H:i:s')
            ],[
                'id'    => 8,
                'm_nama' => 'Pengeluaran Sumber Daya Genetik',
                'm_kategori' => 'Sumber Daya Genetik',
                'm_ditjen' => 'litbang',
                'status' => true,
                'created_at' => date('Y-m-d H:i:s')
            ]
        );
    }
}
